# For a tutorial about this file see
# https://packaging.python.org/tutorials/distributing-packages/
from setuptools import setup
import codecs
import os.path

# __file__ is a variable containing this module's path relative to the current dir
# we use it to construct the path to README.rst
here = os.path.abspath(os.path.dirname(__file__))
readme = os.path.join(here, 'README.rst')

# notice that we open text files with codecs.open
# *Always* open text files with an explicit encoding

with codecs.open(readme, encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='pyed_memdb', # encouraged to be similar to the package name
    version='0.1.0', # semantic
    description='A toy in memory db',
    long_description=long_description,
    url='https://bitbucket.org/mihand/pyed_memdb',
    license='MIT',
    packages=['pyed_memdb'],
    # install_requires=[],  # a list of dependencies other than the standard lib
    extras_require={
        'test': ['pytest'],
    },
    #package_data={}
    #entry_points={}
)