"""
A simple in memory database.

>>> people = table('id', 'name', pk='id', name='people')
>>> people.insertall([
...     (1, 'mihai'),
...     (2, 'ovidiu')
... ])
>>> pprint(people)
+ ----------- +
|    people   |
+ -- + ------ +
| id | name   |
+ -- + ------ +
| 1  | mihai  |
| 2  | ovidiu |
+ -- + ------ +
>>> expertise = table('person_id', 'domain')
>>> expertise.insertall([
...        (1, 'python'),
...        (1, 'numpy'),
...        (2, 'python'),
...        (2, 'f#'),
...    ])
>>> joined = join(people, expertise, 'id', 'person_id', name='people_expertise')
>>> pprint(joined)
+ -------------------------------- +
|         people_expertise         |
+ -- + ------ + --------- + ------ +
| id | name   | person_id | domain |
+ -- + ------ + --------- + ------ +
| 1  | mihai  | 1         | python |
| 1  | mihai  | 1         | numpy  |
| 2  | ovidiu | 2         | python |
| 2  | ovidiu | 2         | f#     |
+ -- + ------ + --------- + ------ +
>>> pprint(joined.select('name', 'domain', domain='python'))
+ --------------- +
|      table      |
+ ------ + ------ +
| name   | domain |
+ ------ + ------ +
| mihai  | python |
| ovidiu | python |
+ ------ + ------ +
>>> pprint(joined.select(domain='python'))
+ -------------------------------- +
|              table               |
+ -- + ------ + --------- + ------ +
| id | name   | person_id | domain |
+ -- + ------ + --------- + ------ +
| 1  | mihai  | 1         | python |
| 2  | ovidiu | 2         | python |
+ -- + ------ + --------- + ------ +
>>> pprint(joined.select('name', 'domain'))
+ --------------- +
|      table      |
+ ------ + ------ +
| name   | domain |
+ ------ + ------ +
| mihai  | python |
| mihai  | numpy  |
| ovidiu | python |
| ovidiu | f#     |
+ ------ + ------ +
"""

from table import table, pprint
from joins import join

