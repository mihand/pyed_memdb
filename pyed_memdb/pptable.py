from collections import defaultdict


def column_widths(rows, header):
    # using a dict cause we accept rows of variing widths
    widths = defaultdict(int)

    def _update(r):
        for ci, c in enumerate(r):
            widths[ci] = max(widths[ci], len(str(c)))

    if header is not None:
        _update(header)

    for r in rows:
        _update(r)
    # now we know how many colums we have. We can collapse to a list
    return [widths[ci] for ci in sorted(widths)]


def table_print(rows, header=None, title=''):

    widths = column_widths(rows, header)

    def delim():
        print '+',
        for w in widths:
            print '-' * w,
            print '+',
        print

    def print_row(r):
        print '|', 
        for c, cw in zip(r, widths):
            print str(c).ljust(cw),
            print '|',
        print 

    if title:
        tot_width = sum(widths) + (len(widths) - 1) * 3
        print '+', '-' * tot_width, '+'
        print '|', str(title).center(tot_width), '|'

    delim()

    if header is not None:
        print_row(header)

    delim()

    for r in rows:
        print_row(r)

    delim()


def test_table_print():
    table_print([
        ('ana', 'are', 'mere'),
        ('iulia', 'are', 'pere'),
        ('andrei dondonoi', 'manca', 'mere'),
        ('petre', 'roade', 'o gulie tare'),
        ],
        header=('nume', 'actiune', 'obiect'),
        title='testing table'
    )
