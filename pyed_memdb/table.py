from pyed_memdb.pptable import table_print


class Row(object):
    """
    Wraps a tuple containing row data.
    Provides a nice read syntax:
    row[4] or row.name
    Can be iterated.
    Can be concatenated. If there are columns named the same they are going to be renamed.
    """

    def __init__(self, data, columns):
        self._data = tuple(data)
        # a map from column name to column index
        self._columns = {c: i for i, c in enumerate(columns)}

    def _asdict(self):
        r = {}
        for col, col_idx in self._columns.iteritems():
            r[col] = self._data[col_idx]
        return r

    def __str__(self):
        colstr = ', '.join('%s=%s' % it for it in self._asdict().iteritems())
        return 'Row(%s)' % colstr

    def __getattr__(self, column_name):
        "implements row.colum_name"
        if column_name in self._columns:
            col_idx = self._columns[column_name]
            return self._data[col_idx]
        raise AttributeError(column_name)

    def __len__(self):
        return len(self._data)

    # this makes Row a iterable. col_idx is a consecutive int
    def __getitem__(self, col_idx):
        "implements row[column_index]"
        # _data will trow the indexerror for us. No need for extra index check
        return self._data[col_idx]

    def __add__(self, other):
        """
        Concatenates 2 rows.
        Columns from the right hand side that appear in the left side are renamed.
        """
        if not isinstance(other, Row):
            raise TypeError('cannot concat row with %s' % other)

        cols = self._columns.copy()
        # rename right columns
        for c, i in other._columns.iteritems():
            new_c = c
            for attempt in xrange(len(cols)):
                if new_c not in cols:
                    break
                new_c = c + str(attempt)
            cols[new_c] = i + len(self._columns)

        data = self._data + other._data
        ret = Row(data, [])
        ret._columns = cols
        return ret

    def row_matches(self, **kwargs):
        "implements a where test for a row"
        return all(getattr(self, col_name) == kwargs[col_name] for col_name in kwargs)

    def select_cols(self, *args):
        "returns a row containing only the selected columns"
        if not args:
            return self
        data = [getattr(self, c) for c in args]
        return Row(data, args)


class Table(object):
    # this does not work in python 2
    # __init__(self, *columns, name=None)
    # *args have to come after positional and default arguments
    #
    # __init__(self, name=None, *columns) would work but not how we expect it
    # Table('a', 'b', 'c') --> name='a', *columns = ['b', 'c']
    #
    # Solution: use kwargs for the options, and extract options manually
    def __init__(self, *columns, **kwargs):
        name = kwargs.pop('name', 'table')
        if kwargs:
            raise ValueError('unexpected kwargs %s' % kwargs)
        if not columns:
            raise ValueError('at least one column needed')
        if not all(isinstance(c, str) for c in columns):
            raise ValueError('columns must be strings')

        self.name = name
        self.columns = columns
        self.rows = []

    def _check_row(self, r):
        if len(r) != len(self.columns):
            raise ValueError('row %s does not match column length' % r)

    def insertall(self, rows):
        # pre insert checks
        for r in rows:
            self._check_row(r)
        # insert
        for r in rows:
            self.rows.append(Row(r, self.columns))

    def _where(self, **kwargs):
        "a scan for rows matching the selectors"
        result = []
        for r in self.rows:
            if r.row_matches(**kwargs):
                result.append(r)
        return result

    def select(self, *args, **kwargs):
        " select arguments where kwargs "
        bad_kwargs = set(kwargs) - set(self.columns)
        if bad_kwargs:
            raise ValueError('no such columns %s' % bad_kwargs)
        bad_args = set(args) - set(self.columns)
        if bad_args:
            raise ValueError('no such columns %s' % bad_args)

        result = []
        for r in self._where(**kwargs):
            result.append(r.select_cols(*args))
        if not args:
            args = self.columns
        ret = Table(*args)
        ret.rows = result
        return ret

    def __str__(self):
        cols = ', '.join(self.columns)
        return 'Table(%s) %d rows' % (cols, len(self.rows))

    # read only list like
    def __len__(self):
        return len(self.rows)

    def __getitem__(self, item):
        return self.rows[item]


class IndexedTable(Table):
    """
    A table that has a primary key.
    It has efficient access to that column
    """

    def __init__(self, *columns, **kwargs):
        pk_name = kwargs.pop('pk')

        if pk_name not in columns:
            raise ValueError('pk is not a column')

        Table.__init__(self, *columns, **kwargs)
        self.pk_name = pk_name
        self.pki = columns.index(pk_name)
        self.pk_index = {}

    def insertall(self, rows):
        # pre insert checks
        for r in rows:
            Table._check_row(self, r)
            r_pk = r[self.pki]
            if r_pk in self.pk_index:
                raise ValueError('row with pk %s allready in table' % r_pk)
            hash(r_pk)  # check to see if pk is hashable
        # insert
        for r in rows:
            self.rows.append(Row(r, self.columns))
            # update primary key index
            r_pk = r[self.pki]
            self.pk_index[r_pk] = len(self.rows) - 1

    def __str__(self):
        cols = ', '.join(self.columns)
        pk = self.pk_name
        return 'IndexedTable(%s, pk=%s) %d rows' % (cols, pk, len(self.rows))

    def _where(self, **kwargs):
        # see if we filter after the primary key
        if self.pk_name in kwargs:
            pk = kwargs.pop(self.pk_name)
            if pk not in self.pk_index:
                # no row with this pk value
                return []
            row_idx = self.pk_index[pk]
            r = self.rows[row_idx]
            if r.row_matches(**kwargs):
                return [r]
            return []
        # revert to table scan
        return Table._where(self, **kwargs)


def table(*columns, **kwargs):
    if 'pk' in kwargs:
        return IndexedTable(*columns, **kwargs)
    else:
        return Table(*columns, **kwargs)


def pprint(t):
    table_print(t, t.columns, t.name)
