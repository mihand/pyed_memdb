from pyed_memdb.table import Table, IndexedTable


def _hash_join(t1, t2, col1, col2, swapped=False):
    """
    efficient join. Uses len(t1) operations
    Needs t2 to be indexed on col2
    If swapped is true then it does the join of
    t2 with t1 and t1 must be indexed on col1
    """
    if swapped:
        col1, col2 = col2, col1
        t1, t2 = t2, t1
    assert isinstance(t2, IndexedTable)
    assert col2 == t2.pk_name
    result = []
    for r1 in t1.rows:
        pk = getattr(r1, col1)
        if pk in t2.pk_index:
            idx = t2.pk_index[pk]
            r2 = t2.rows[idx]
            new_row = r2 + r1 if swapped else r1 + r2
            result.append(new_row)
    return result


def _cross_join(t1, t2, col1, col2):
    """
    A generic slow join. Used len(t1) * len(t2) operations
    """
    result = []
    for r1 in t1.rows:
        for r2 in t2.rows:
            if getattr(r1, col1) == getattr(r2, col2):
                result.append(r1 + r2)
    return result


# another effect of defaults evaluating at import time
# you cannot do this:
# def join(t1, t2, col1, col2=col1)

def join(t1, t2, col1, col2=None, name=None):
    """ joins the two tables. Returns a new table """
    if col2 is None:
        col2 = col1

    # Choose a hash strategy.
    # t1_indexed  if t1 is indexed on col1
    # t1_indexed & t2_indexed: hash join starting with the smaller index
    # one indexed: hash join the unindexed with the indexed
    # no one indexed: cross join

    def _have_index(t, col):
        return isinstance(t, IndexedTable) and t.pk_name == col

    t1_indexed = _have_index(t1, col1)
    t2_indexed = _have_index(t2, col2)

    if t1_indexed and t2_indexed:
        # hash join the smaller against the larger
        swapped = len(t1) > len(t2)
        result = _hash_join(t1, t2, col1, col2, swapped=swapped)
    elif t1_indexed:
        result = _hash_join(t1, t2, col1, col2, swapped=True)
    elif t2_indexed:
        result = _hash_join(t1, t2, col1, col2)
    else:
        result = _cross_join(t1, t2, col1, col2)

    ret = Table(*(t1.columns + t2.columns), name=name)
    ret.rows = result
    return ret

