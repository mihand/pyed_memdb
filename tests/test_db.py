import pytest

from pyed_memdb import table, pprint, join
from pyed_memdb.table import Row, Table, IndexedTable


def test_row():
    r = Row(list('xyz'), columns='abc')
    assert r[1] == r.b
    assert list(r) == list('xyz')
    assert str(r)


def test_table_creation():
    t = Table('foo', 'bar')
    t2 = IndexedTable('id', 'name', pk='id')
    assert t is not None
    assert t2 is not None
    assert not t  # this will call len to bool convert table
    assert not t2
    t.insertall([('a', 'o')])
    t2.insertall([('3', 'o')])
    assert len(t) == 1


@pytest.fixture
def db_sample():
    people = table('id', 'name', pk='id')
    people.insertall([
        (1, 'mihai'),
        (2, 'ovidiu')
    ])
    expertise = table('person_id', 'domain')
    expertise.insertall([
        (1, 'python'),
        (1, 'numpy'),
        (2, 'python'),
        (2, 'f#'),
    ])
    return people, expertise


def test_select(db_sample):
    people, expertise = db_sample
    print people.select(id=1).name


def test_join(db_sample):
    people, expertise = db_sample
    tj = join(people, expertise, 'id', 'person_id')
    assert len(tj) == 4


def test_complex_select(db_sample):
    people, expertise = db_sample
    tj = join(people, expertise, 'id', 'person_id')

    s2 = tj.select('name', 'domain', domain='python')
    assert len(s2) == 2
    assert len(s2.columns) == 2

    s1 = tj.select(domain='python')
    assert len(s1) == 2
    assert s1.columns == ('id', 'name', 'person_id', 'domain')
    assert s1[1]._data == (2, 'ovidiu', 2, 'python')


def test_non_indexed_join(db_sample):
    people, expertise = db_sample
    tj = join(people, expertise, 'id', 'person_id')
    # a select will drop indices
    a = people.select('id')
    s = join(tj, a, 'id')


def test_full_indexed_join():
    # todo
    pass


def test_print(db_sample):
    people, expertise = db_sample
    pprint(people)
    tj = join(people, expertise, 'id', 'person_id')
    pprint(tj)
