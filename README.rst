memdb
=====

This package implements a simplistic in memory data base.
It is a teaching sample not a utility fit for production use.
Instead use in memory sqlite, or similar stuff.

Installing
----------

.. code-block:: bash

    $ pip install git+git://git@bitbucket.org/mihand/pyed_memdb.git

Usage
-----

See help(pyed_memdb)



